#ifndef main_h
#define main_h

#include <stdint.h>
#include <Servo.h>
#include <Wire.h>
#include "CayenneLPP.h"
#include "lmic.h"
#include "Max44009.h"
#include "configuration.h"

// Enumerations
// Enum
enum doorStates_t  {
    doorClosed,
    doorOpened,
    doorUndefined
};

// Structures
struct luxSata_s {
    uint16_t value;
    int error;
};

// Globals
uint16_t txInterval;
Servo servoDoorHolder;
uint8_t doorState;
float doorMovementTimeUpS;
float doorMovementTimeDownS;

// Objects
static osjob_t sendjob;
CayenneLPP lpp(51);
Max44009 luxSensor(luxSensorAddress);

// Functions
void onEvent (ev_t ev);
void do_send(osjob_t* j);
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8); }
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8); }
void os_getDevKey (u1_t* buf) { memcpy_P(buf, APPKEY, 16); }

float readVoltage_f(int pin);
void low_power_delay_s(float txInterval);
void updateLuxSensor(luxSata_s *sensor);
void doorClose();
void doorOpen();
void doorUnlock();
void doorLock();


// Activate/desactivate debug with only one #define
// From low power lab :)
#ifdef DEBUG_ON
  #define DEBUG( input )    { Serial.print(input); }
  #define DEBUGDec( input ) { Serial.print( input, DEC); }
  #define DEBUGHex( input ) { Serial.print(input, HEX); }
  #define DEBUGln( input )  { Serial.println(input); }
  #define DEBUG_FLUSH() { Serial.flush(); }
#else
  #define DEBUG( input )
  #define DEBUGDec( input )
  #define DEBUGHex( input )
  #define DEBUGln(input)
  #define DEBUG_FLUSH()
#endif
#endif