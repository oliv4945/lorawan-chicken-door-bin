#ifndef configuration_h
#define configuration_h

#include <stdint.h>
#include <Arduino.h>
#include "hal/hal.h"
#include "lmic.h"
#include "LowPower.h"

// Defines
#define USE_WATCHDOG
#define DEBUG_ON

// Enum


// Constants
const uint32_t i2c_clock_hz = 100000;

const uint8_t pinVCC = A3;
const uint8_t pinPowerOn = 4;
const uint8_t pinServo = 5;
const uint8_t pinMotorSpeed = 7;    // A
const uint8_t pinMotorDir   = 8;    // B

const uint8_t luxSensorAddress   = 0xCA;
const uint16_t defaultTxInterval = 5 * 60;
const float battery_calibration = 0.006395549;
const float delayServoMvmtMs = 200;
uint16_t luxThreshold = 2;
const uint32_t door_mvmt_timeout_ms = 5500;

const uint8_t lppChannelVoltage = 0;
const uint8_t lppChannelLuxValue = 1;
const uint8_t lppChannelLuxError = 2;
const uint8_t lppChannelDoorState = 3;

const uint8_t uplinkFport = 4;

// Pins
const uint8_t pinSx1276NSS  = 10;

// Structures



// LoRaWAN configuration
// Little-endian format, LSB first
// For TTN issued APPEUI, the last bytes should be 0xD5, 0xB3,0x70.
static const u1_t PROGMEM APPEUI[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0xD5, 0xB3, 0x70 };
static const u1_t PROGMEM DEVEUI[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// Big endian format
static const u1_t PROGMEM APPKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = pinSx1276NSS,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = LMIC_UNUSED_PIN,
    .dio = {2, 3, LMIC_UNUSED_PIN}
};

#endif