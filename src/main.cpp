#include <Arduino.h>
#include <SPI.h>
#include "main.h"
#include "configuration.h"
#include "Wire.h"
#include "LowPower.h"
#include "CayenneLPP.h"
#include "lmic.h"
#include "hal/hal.h"
#include "lmic/oslmic.h"
#ifdef USE_WATCHDOG
#include <avr/wdt.h>
#endif

// Arduino setup procedure
// Called once at startup
void setup() {
#ifdef USE_WATCHDOG
    // Watchdog
    wdt_enable(WDTO_8S);
    wdt_reset();
#endif

    pinMode(pinPowerOn, OUTPUT);
    digitalWrite(pinPowerOn, LOW);
    pinMode(pinMotorSpeed, OUTPUT); // A
    pinMode(pinMotorDir, OUTPUT);   // B
    digitalWrite(pinMotorSpeed, LOW);
    digitalWrite(pinMotorDir, LOW);
    servoDoorHolder.attach(pinServo);
    

    // Serial init
#ifdef DEBUG_ON
    Serial.begin(57600);
#endif
    DEBUG(F("Starting\n"));

    // Time of flight sensor init
    tof.init();
    tof.setTimeout(vl53l0xTimingRecUs*2/1000);
    tof.setMeasurementTimingBudget(vl53l0xTimingRecUs);

    txInterval = defaultTxInterval;
    doorState = doorClosed;             // Always assume closed door at start


    // Set I2C clock speed
    Wire.setClock(i2c_clock_hz);

    // LMIC init
#ifdef USE_WATCHDOG
    wdt_reset();
#endif
    // Init LoRaWAN stack
    os_init();
    LMIC_reset();
    LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);
    // LMIC_setDrTxpow(DR_SF12, 14);
    LMIC_setAdrMode(1);
    // Start job
    do_send(&sendjob);
}

// Called by Arduino after setup()
void loop() {
    // local variables

    // Actual loop
    while (1) {

#ifdef USE_WATCHDOG
        // Clear watchdog
        wdt_reset();
#endif
        // Update mac
        os_runloop_once();
    }
}


void onEvent (ev_t ev) {
#ifdef USE_WATCHDOG
    wdt_reset();
#endif
    DEBUG(os_getTime());
    DEBUG(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            DEBUGln(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_JOINING:
            DEBUGln(F("EV_JOINING"));
            break;
        case EV_JOINED:
            DEBUGln(F("EV_JOINED"));
            /*{
              
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              DEBUG("netid: ");
              DEBUGDec(netid);
              DEBUG("\ndevaddr: ");
              DEBUGHex(devaddr);
              DEBUG("\nartKey: ");
              for (uint8_t i=0; i<sizeof(artKey); ++i) {
                DEBUGHex(artKey[i]);
              }
              DEBUGln("");
              DEBUG("nwkKey: ");
              for (uint8_t i=0; i<sizeof(nwkKey); ++i) {
                DEBUGHex(nwkKey[i]);
              }
              DEBUGln("");
            }*/
            // Disable link check validation.
            LMIC_setLinkCheckMode(0);
            break;
        case EV_JOIN_FAILED:
            DEBUGln(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            DEBUGln(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            DEBUGln(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              DEBUGln(F("Received ack"));
            if (LMIC.dataLen > 0) {
                DEBUG(F("Received "));
                DEBUG(LMIC.dataLen);
                DEBUGln(F(" bytes of payload"));
                // Fport 1
                if (LMIC.frame[LMIC.dataBeg-1] == 1) {
                    txInterval = (LMIC.frame[LMIC.dataBeg] << 8) + LMIC.frame[LMIC.dataBeg + 1];
                    DEBUG("New sleep time:");
                    DEBUGln(txInterval);
                // Fport 2
                } else if (LMIC.frame[LMIC.dataBeg-1] == 2) {
                    luxThreshold = LMIC.frame[LMIC.dataBeg];
                    DEBUG("New luxThreshold:");
                    DEBUGln(luxThreshold);
                }
            }
            // Enter in sleep mode
            low_power_delay_s( (float) txInterval );

            // Schedule next transmission
            // os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            // Or send now
            do_send(&sendjob);
            break;
        case EV_LOST_TSYNC:
            DEBUGln(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            DEBUGln(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            DEBUGln(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            DEBUGln(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            DEBUGln(F("EV_LINK_ALIVE"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_SCAN_FOUND:
        ||    DEBUGln(F("EV_SCAN_FOUND"));
        ||    break;
        */
        /*case EV_TXSTART:
            DEBUGln(F("EV_TXSTART"));
            break;*/
        default:
            DEBUG(F("Unknown event: "));
            DEBUGln((unsigned) ev);
            break;
    }
}


void do_send(osjob_t* j) {
    luxSata_s luxData;

#ifdef USE_WATCHDOG
    wdt_reset();
#endif

    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        DEBUGln(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        updateLuxSensor(&luxData);
        // Change door state
        if (luxData.error == 0) {
            if ((doorState != doorClosed) && (luxData.value <= luxThreshold)) {
                doorClose();
            }
            if ((doorState != doorOpened) && (luxData.value > luxThreshold)) {
                doorOpen();
            }
        }

        lpp.reset();
        lpp.addAnalogOutput(lppChannelVoltage, readVoltage_f(pinVCC));
        lpp.addLuminosity(lppChannelLuxValue, luxData.value);
        lpp.addDigitalOutput(lppChannelLuxError, luxData.error);
        lpp.addDigitalInput(lppChannelDoorState, doorState);

        // LMIC_setTxData2 (u1_t port, xref2u1_t data, u1_t dlen, u1_t confirmed)
        LMIC_setTxData2(uplinkFport, lpp.getBuffer(), lpp.getSize(), false);
        DEBUGln(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

float readVoltage_f(int pin) {
#ifdef USE_WATCHDOG
    wdt_reset();
#endif
    float voltage;
    // Vin = analogRead(pin)*3.3/1024*2*417/406; // Read value*3.3/1024, Times 2 for resistors, *417/406 for calibration
    voltage = ((float) analogRead(pin)) * battery_calibration; // TBC - Add calibration value in configuration.h
    delay(1); // Time for ADC to settle
    voltage = ((float) analogRead(pin)) * battery_calibration;
    return voltage;
}


void low_power_delay_s(float tx_interval_float) {
    // Integer as ( tx_interval_ms - sleep_time ) can be lower than 0
    int16_t sleep_time = 0;
    int16_t tx_interval_s = (int16_t) tx_interval_float;

    int16_t tx_interval_ms = ( tx_interval_float - ((float) tx_interval_s) ) * 1000;
    ASSERT( tx_interval_ms < 1000 );

    // Flush TX buffer if required
    DEBUG_FLUSH();

#ifdef USE_WATCHDOG
    wdt_reset();
    wdt_disable();
#endif
    
    while ( sleep_time < tx_interval_s ) {
        switch ( tx_interval_s - sleep_time ) {
            case 1:
                LowPower.powerDown( SLEEP_1S, ADC_OFF, BOD_ON );
                sleep_time += 1;
                break;
            case 2:
            case 3:
                LowPower.powerDown( SLEEP_2S, ADC_OFF, BOD_ON );
                sleep_time += 2;
                break;
            case 4:
            case 5:
            case 6:
            case 7:
                LowPower.powerDown( SLEEP_4S, ADC_OFF, BOD_ON );
                sleep_time += 4;
                break;
            default:
                LowPower.powerDown( SLEEP_8S, ADC_OFF, BOD_ON );
                sleep_time += 8;
        }
    }

    sleep_time = 0;
    while ( ( tx_interval_ms - sleep_time ) >= 500 ) {
        sleep_time += 500;
        LowPower.powerDown( SLEEP_500MS, ADC_OFF, BOD_ON );
    }
    while ( ( tx_interval_ms - sleep_time ) >= 250 ) {
        sleep_time += 250;
        LowPower.powerDown( SLEEP_250MS, ADC_OFF, BOD_ON );
    }
    while ( ( tx_interval_ms - sleep_time ) >= 120 ) {
        sleep_time += 120;
        LowPower.powerDown( SLEEP_120MS, ADC_OFF, BOD_ON );
    }
    while ( ( tx_interval_ms - sleep_time ) >= 60 ) {
        LowPower.powerDown( SLEEP_60MS, ADC_OFF, BOD_ON );
        sleep_time += 60;
    }
    while ( ( tx_interval_ms - sleep_time ) >=  30 ) {
        LowPower.powerDown( SLEEP_30MS, ADC_OFF, BOD_ON );
        sleep_time += 30;
    }
    ASSERT( ( tx_interval_ms - sleep_time ) < 30 );

#ifdef USE_WATCHDOG
    wdt_enable(WDTO_4S);
    wdt_reset();
#endif
}

void updateLuxSensor(luxSata_s *data) {
#ifdef USE_WATCHDOG
    wdt_reset();
#endif
    data->value = (uint16_t) luxSensor.getLux();
    data->error = luxSensor.getError();
    DEBUG(F("Lux: "));
    DEBUG(data->value);
    DEBUG(F(" lux - error: "));
    DEBUGln(data->error);
}


void doorOpen() {
#ifdef USE_WATCHDOG
    wdt_reset();
#endif
    DEBUGln(F("Open"));
    uint32_t time_start;
    bool timeout = false;
    doorMovementTimeUpS = 3.0;
    doorState = doorOpened;

    // Start movement
    digitalWrite(pinPowerOn, HIGH);
    digitalWrite(pinMotorSpeed, HIGH);

    // Move X seconds before probing sensor
    low_power_delay_s(doorMovementTimeUpS);

    // Move until door is opened (first loop distance_avg=0 to enter inside)
    time_start = millis();
    while ((state_door_switch == true) && (timeout == false)) {
        if ((millis() - time_start) > door_mvmt_timeout_ms) {
            timeout = true;
        }
        // TODO - Add switch reading
        low_power_delay_s(0.125);
    }

    // Stop movement
    digitalWrite(pinMotorSpeed, LOW);
    digitalWrite(pinPowerOn, LOW);
}

void doorClose() {
#ifdef USE_WATCHDOG
    wdt_reset();
#endif
    DEBUGln(F("Close"));
    uint32_t time_start;
    bool timeout = false;
    doorMovementTimeDownS = 3.0;
    doorState = doorClosed;

    // Start movement
    digitalWrite(pinPowerOn, HIGH);
    digitalWrite(pinMotorDir, HIGH);

    // Move X seconds before probing sensor
    low_power_delay_s(doorMovementTimeDownS);

    // Move until door is closed (first loop distance=0 to enter inside)
    time_start = millis();
    while ((state_door_switch == true) && (timeout == false)) {
        if ((millis() - time_start) > door_mvmt_timeout_ms) {
            timeout = true;
        }
        // TODO - Add switch reading
        low_power_delay_s(0.125);
    }
    
    // Stop movement
    digitalWrite(pinMotorDir, LOW);
    digitalWrite(pinPowerOn, LOW);
}
